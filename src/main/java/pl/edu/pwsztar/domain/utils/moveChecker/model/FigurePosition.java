package pl.edu.pwsztar.domain.utils.moveChecker.model;

public class FigurePosition {

    private final String horizontal;
    private final String vertical;

    public FigurePosition(String horizontal, String vertical) {
        this.horizontal = horizontal;
        this.vertical = vertical;
    }

    public String getHorizontal() {
        return this.horizontal;
    }

    public String getVertical() {
        return this.vertical;
    }
}
